from django.urls import path

from api.users.views import LoggedUserViewSet

app_name = "users"
urlpatterns = [
    path("users/me/", LoggedUserViewSet.as_view(), name="me"),
]
