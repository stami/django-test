import pytest

from api.users.models import CustomUser
from api.users.tests.factories import UserFactory


@pytest.fixture
def user_fixture(db) -> CustomUser:
    return UserFactory()
