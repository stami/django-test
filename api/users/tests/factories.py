from typing import Any, Sequence

import django.contrib.auth.models as auth_models
from django.conf import settings
from django.contrib.auth import get_user_model
from factory import Faker, post_generation
from factory.django import DjangoModelFactory


class UserFactory(DjangoModelFactory):

    email = Faker("email")

    @post_generation
    def password(self, create: bool, extracted: Sequence[Any], **kwargs):
        password = (
            extracted
            if extracted
            else Faker(
                "password",
                length=42,
                special_chars=True,
                digits=True,
                upper_case=True,
                lower_case=True,
            ).evaluate(None, None, extra={"locale": None})
        )
        self.set_password(password)

    class Meta:
        model = get_user_model()
        django_get_or_create = ["email"]


class AddUserViewSetFactory(DjangoModelFactory):

    email = Faker("email")
    password = Faker("password")

    class Meta:
        model = get_user_model()
        django_get_or_create = ["email"]


class AddUserViewSetInvalidFactory(DjangoModelFactory):

    name = Faker("name")

    class Meta:
        model = get_user_model()
        django_get_or_create = ["email"]


class CommonUserGroupFactory(DjangoModelFactory):
    class Meta:
        model = auth_models.Group

    name = settings.APP_USERS_GROUPS["common_users"]["name"]
