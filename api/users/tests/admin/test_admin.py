import pytest
from django.urls import reverse

from api.users.models import CustomUser
from api.users.tests.factories import UserFactory

pytestmark = pytest.mark.django_db


class TestUserAdmin:
    def test_changelist(self, admin_client):
        url = reverse("admin:users_customuser_changelist")
        response = admin_client.get(url)
        assert response.status_code == 200

    def test_search(self, admin_client):
        url = reverse("admin:users_customuser_changelist")
        response = admin_client.get(url, data={"q": "test"})
        assert response.status_code == 200

    def test_add(self, admin_client):
        url = reverse("admin:users_customuser_add")
        response = admin_client.get(url)
        assert response.status_code == 200

        response = admin_client.post(
            url,
            data={
                "email": "test@domain.com",
                "password1": "My_R@ndom-P@ssw0rd",
                "password2": "My_R@ndom-P@ssw0rd",
            },
        )
        assert response.status_code == 302
        assert CustomUser.objects.filter(email="test@domain.com").exists()

    def test_view_user(self, admin_client):
        tmp_user = UserFactory()
        user = CustomUser.objects.get(email=tmp_user.email)
        url = reverse("admin:users_customuser_change", kwargs={"object_id": user.id})
        response = admin_client.get(url)
        assert response.status_code == 200
