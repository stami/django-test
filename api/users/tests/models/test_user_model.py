import pytest
from django.db.models import Q

from api.users.models import CustomUser
from api.users.tests.factories import UserFactory
from api.users.tests.fixtures import user_fixture

pytestmark = pytest.mark.django_db
test_user = user_fixture


class TestUserModel:
    def test_user_add(self):
        user = UserFactory()
        assert user.id != ""

    def test_read_db(self, test_user: CustomUser):
        instance = CustomUser.objects.get(id=test_user.id)
        assert test_user == instance

    def test_get_objects(self):
        UserFactory.create_batch(4)

        assert CustomUser.objects.filter(Q(is_superuser=False)).count() == 4

    def test_soft_delete(self):
        UserFactory.create_batch(4)
        user_to_delete = UserFactory()

        assert CustomUser.objects.filter(Q(is_superuser=False)).count() == 5

        id_to_check = str(user_to_delete.id)
        user_to_delete.delete()

        assert CustomUser.objects.filter(Q(is_superuser=False)).count() == 4
        assert CustomUser.all_objects.filter(Q(is_superuser=False)).count() == 5
        assert CustomUser.objects.filter(Q(id=str(id_to_check))).count() == 0
        assert CustomUser.all_objects.filter(Q(id=str(id_to_check))).count() == 1

    def test_hard_delete(self):
        UserFactory.create_batch(4)
        user_to_delete = UserFactory()

        assert CustomUser.objects.filter(Q(is_superuser=False)).count() == 5

        id_to_check = str(user_to_delete.id)
        user_to_delete.hard_delete()

        assert CustomUser.objects.filter(Q(is_superuser=False)).count() == 4
        assert CustomUser.all_objects.filter(Q(is_superuser=False)).count() == 4
        assert CustomUser.objects.filter(Q(id=str(id_to_check))).count() == 0
        assert CustomUser.all_objects.filter(Q(id=str(id_to_check))).count() == 0
