import factory

from api.users.serializers import UserSerializer
from api.users.tests.factories import (
    AddUserViewSetFactory,
    AddUserViewSetInvalidFactory,
    CommonUserGroupFactory,
    UserFactory,
)
from api.users.tests.fixtures import user_fixture

test_user = user_fixture


class TestUserSerializer:
    def test_serialize_model(self, db):
        user = UserFactory.create()
        group = CommonUserGroupFactory.create()
        user.groups.add(group)
        serializer = UserSerializer(user)

        assert serializer.data
        assert set(serializer.data.keys()) == set(
            [
                "status",
                "is_active",
                "id",
                "email",
                "name",
                "groups",
                "birthdate",
                "last_login",
                "date_joined",
            ]
        )

    def test_serialized_data(self, db):
        valid_serialized_data = factory.build(dict, FACTORY_CLASS=AddUserViewSetFactory)
        serializer = UserSerializer(data=valid_serialized_data)
        serializer.is_valid()
        assert serializer.is_valid()
        assert serializer.errors == {}

        invalid_serialized_data = factory.build(
            dict, FACTORY_CLASS=AddUserViewSetInvalidFactory
        )
        serializer = UserSerializer(data=invalid_serialized_data)
        serializer.is_valid()
        assert serializer.is_valid() is False
        assert serializer.errors != {}
