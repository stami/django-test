import ast
import base64

import pytest
from django.urls import reverse
from faker import Faker
from rest_framework.test import APIClient

from api.users.tests.factories import UserFactory

pytestmark = pytest.mark.django_db


class TestJWTViews:
    def setup_class(self):
        self.test_email = "jwt_test_user@email.pl"
        self.test_pass = "TestPass123"

        self.api_client = APIClient()
        self.faker = Faker()

    def __create_test_user(self):
        return UserFactory(email=self.test_email, password=self.test_pass)

    def __token_decode(self, token):
        token = token.split(".")[1]
        decoded_token = ast.literal_eval(
            base64.b64decode(token + "=" * (-len(token) % 4)).decode("utf-8")
        )
        return decoded_token

    def test_token_obtain_pair(self):
        ep = reverse("token_obtain_pair")
        self.__create_test_user()
        user_credentials = {"email": self.test_email, "password": self.test_pass}
        response = self.api_client.post(ep, data=user_credentials, format="json")
        assert response.data.get("access")
        assert response.data.get("refresh")

        token_data = self.__token_decode(response.data.get("access"))
        assert "user_groups" in token_data

    def test_token_obtain_pair_bad_credentials(self):
        ep = reverse("token_obtain_pair")
        user_credentials = {"email": "random@email.com", "password": "NotExistPassword"}
        response = self.api_client.post(ep, data=user_credentials, format="json")
        assert response.status_code == 401

    def test_token_refresh(self):
        ep_obtain_token = reverse("token_obtain_pair")  # 1. Get user tokens
        self.__create_test_user()
        user_credentials = {"email": self.test_email, "password": self.test_pass}
        response_obtain_token = self.api_client.post(
            ep_obtain_token, data=user_credentials, format="json"
        )

        ep_refresh = reverse("token_refresh")  # 2. Check refresh tokens
        response = self.api_client.post(
            ep_refresh,
            data={"refresh": response_obtain_token.data["refresh"]},
            format="json",
        )
        assert response.data.get("access")

        # 3. Check bad refresh token
        response = self.api_client.post(
            ep_refresh,
            data={"refresh": self.faker.sha256()},
            format="json",
        )
        assert response.status_code == 401

    # def test_token_validity(self):
    # TODO: After add first secured api view different than obtain token or token refresh
    #     token = RefreshToken.for_user(self.test_email)
    #     secured_ep = reverse("...")

    #     client = self.api_client
    #     client.credentials(
    #         HTTP_AUTHORIZATION="Bearer {}".format(str(token.access_token))
    #     )
    #     response = client.get(secured_ep, format="json")
    #     assert response.status_code == 200

    #     # Check invalid access token
    #     client.credentials(
    #         HTTP_AUTHORIZATION="Bearer {}".format(str(self.faker.sha256()))
    #     )
    #     response = client.get(secured_ep, format="json")
    #     assert response.status_code == 403
