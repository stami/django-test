import pytest
from django.contrib.auth import get_user_model
from django.urls import reverse
from faker import Faker
from rest_framework.test import APIClient

from api.users.serializers import UserSerializer
from api.users.tests.fixtures import user_fixture

pytestmark = pytest.mark.django_db
test_user = user_fixture
fake = Faker()
User = get_user_model()


class TestUserDetailsView:
    def setup_class(self):
        self.endpoint = reverse("users:me")
        self.api_client = APIClient()

    def test_retrieve(self, test_user: User):
        self.api_client.force_authenticate(user=test_user)
        serializer = UserSerializer(test_user)

        response = self.api_client.get(self.endpoint)

        assert response.data == serializer.data
        assert response.status_code == 200
