from django.conf import settings
from django.urls import resolve, reverse

from api.users.views import LoggedUserViewSet


class TestUserDetailsUrl:
    def test_retrieve(self):
        """
        GET
        """
        assert reverse("users:me") == f"/api/{settings.API_VERSION}/users/me/"
        assert resolve(f"/api/{settings.API_VERSION}/users/me/").view_name == "users:me"
        assert (
            resolve(f"/api/{settings.API_VERSION}/users/me/").func.cls
            == LoggedUserViewSet
        )
