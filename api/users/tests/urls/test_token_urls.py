import pytest
from django.conf import settings
from django.urls import resolve, reverse

from api.users.tests.fixtures import user_fixture
from api.users.views import CustomTokenObtainPairView

pytestmark = pytest.mark.django_db
test_user = user_fixture


class TestTokenUrls:
    def test_get_token(self):
        assert reverse("token_obtain_pair") == "/api/{}/token/".format(
            settings.API_VERSION
        )
        assert (
            resolve(f"/api/{settings.API_VERSION}/token/").view_name
            == "token_obtain_pair"
        )
        assert (
            resolve(f"/api/{settings.API_VERSION}/token/").func.cls
            == CustomTokenObtainPairView
        )

    def test_token_refresh(self):
        assert reverse("token_refresh") == "/api/{}/token/refresh/".format(
            settings.API_VERSION
        )
        assert (
            resolve(f"/api/{settings.API_VERSION}/token/refresh/").view_name
            == "token_refresh"
        )
