import uuid

from django.conf import settings
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.db.models import CharField
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from api.utils.groups_management import GroupsManagement
from api.utils.send_email import SendEmail
from api.utils.soft_deletable_qs import SoftDeletableQS


class CustomUserManager(BaseUserManager):
    """
    Custom user model manager where email is the unique identifiers
    for authentication instead of usernames.
    """

    def get_queryset(self):
        """returns not deleted Users - common use"""
        return SoftDeletableQS(
            model=self.model, using=self._db, hints=self._hints
        ).filter(deleted_at__isnull=True)

    def get_deleted_users(self, **kwargs):
        """returns deleted Users"""
        return SoftDeletableQS(
            model=self.model, using=self._db, hints=self._hints
        ).filter(deleted_at__isnull=False)

    def create_user(self, email, password, **extra_fields):
        """
        Create and save a User with the given email and password.
        """
        if not email:
            raise ValueError(_("EUSM1: The Email must be set"))
        extra_fields.setdefault("is_active", True)
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        # Assign group to user
        user = GroupsManagement().add_group(
            user, settings.APP_USERS_GROUPS["common_users"]["name"]
        )

        # Send email account confirmation
        email_subject = _("Welcome to Test System")
        email_data = {
            "template_name": "account_create_email",
            "data": {"title": str(email_subject)},
        }
        sender_address = settings.SUPPORT_SENDER
        sender_class = SendEmail()
        sender_class.send(
            [email],
            str(email_subject),
            email_data,
            sender_address,
        )

        return user

    def create_superuser(self, email, password, **extra_fields):
        """
        Create and save a SuperUser with the given email and password.
        """
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_active", True)
        extra_fields.setdefault("status", "confirmed")

        if not extra_fields.get("is_staff"):
            raise ValueError(_("Superuser must have is_staff=True."))
        if not extra_fields.get("is_superuser"):
            raise ValueError(_("Superuser must have is_superuser=True."))

        return self.create_user(email, password, **extra_fields)


class CustomUser(AbstractUser):
    """
    Default user model
    User.objects.all() - returns not deleted users
    User.all_objects.all() - returns all users also deleted one
    """

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    STATUS_UNCONFIRMED = "inactive"
    STATUS_CONFIRMED = "active"
    objects = CustomUserManager()
    all_objects = models.Manager()

    STATUS_CHOICES = (
        (STATUS_UNCONFIRMED, _("Inactive")),
        (STATUS_CONFIRMED, _("Active")),
    )
    status = models.CharField(
        max_length=11, choices=STATUS_CHOICES, default=STATUS_UNCONFIRMED
    )
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False, unique=True
    )
    username = None
    email = models.EmailField(_("Email address"), unique=True)
    name = CharField(_("Name of User"), blank=True, max_length=255)
    deleted_at = models.DateTimeField(null=True)
    birthdate = models.DateField(null=True, blank=True)

    def delete(self):
        """Softly delete object"""
        self.deleted_at = timezone.now()
        self.is_active = False
        self.save()

    def hard_delete(self):
        """Remove the object from the database permanently"""
        super().delete()

    def __str__(self):
        return "{}".format(self.email)
