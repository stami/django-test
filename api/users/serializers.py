from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    groups = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = [
            "status",
            "is_active",
            "id",
            "email",
            "name",
            "groups",
            "birthdate",
            "last_login",
            "date_joined",
        ]

    def get_groups(self, obj):
        return list(obj.groups.values_list("name", flat=True))


class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    default_error_messages = {
        "no_active_account": _("EUSS1: Wrong username or password")
    }

    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        # Add custom data
        token["user_groups"] = list(user.groups.values_list("name", flat=True))
        return token
