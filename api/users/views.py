from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework_simplejwt.views import TokenObtainPairView

from api.users.serializers import CustomTokenObtainPairSerializer, UserSerializer

User = get_user_model()


class LoggedUserViewSet(generics.RetrieveAPIView):
    """
    Get logged user details
    =======================

    # GET
    ## Response
    - `status`: User's current account status
    - `id`: User ID
    - `email`: Logged User email
    - `name`: Logged User name
    - `groups`: Array with User's groups names
    - `birthdate`: __nullable__ User's buirth date
    """

    serializer_class = UserSerializer
    queryset = User.objects.all()
    allowed_update_fields = ["password", "name", "birthdate", "gender"]

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(queryset, id=self.request.user.id)
        return obj


class CustomTokenObtainPairView(TokenObtainPairView):
    # TODO - Add tests for view and urls with token and token refresh
    """
    Token Obtain Pair
    =================
    Returns pair of user token
    --------------------------
    ---
    # POST
    # Payload
    - `email` - user's email
    - `password` - user's password

    # Response
    - `refresh` - Refresh token
    - `access` - Acces token (Bearer)
    """

    serializer_class = CustomTokenObtainPairSerializer
