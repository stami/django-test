import json

from django.conf import settings


class SendEmail:
    """
    In this test task, only print in shell.
    """

    template = None
    environment = getattr(settings, "ENV_ID", None)
    environment_email_subject_prefix = ""
    automatic_tests = False
    if environment:
        if environment == "TESTS":
            automatic_tests = True

        environment_email_subject_prefix = "[{}] ".format(
            environment
        )  # additional space after env identificator in email subject

    def send(
        self,
        recipients,
        subject,
        email_data,
        sender=settings.SUPPORT_SENDER,
    ):

        email = """
        Recipients: {}
        Email subject: {}
        Email data: {}
        Sender: {}
        """.format(
            recipients,
            subject,
            json.dumps(email_data, indent=4, sort_keys=True),
            sender,
        )

        print(email)
