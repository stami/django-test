from django.db import migrations
from django.conf import settings



def createsuperuser(apps, schema_editor):
    """
    Create superuser for new app build
    TODO: Add superuser cration for other environmemts with using secrets
    """
    
    if settings.ENV_ID == "LOCAL":
        admin_password = "admin"
        # Create a new user using acquired password
        from django.contrib.auth import get_user_model

        User = get_user_model()
        User.objects.create_superuser("admin", password=admin_password)


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [migrations.RunPython(createsuperuser)]
