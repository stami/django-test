from django.contrib.auth.models import Group


class GroupsManagement:
    def add_group(self, user, group_name):
        group_to_add, created = Group.objects.get_or_create(name=group_name)
        group_to_add.user_set.add(user)

        return user
