from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response


class CustomPagination(PageNumberPagination):
    def get_paginated_response(self, data):
        return Response(
            {
                "results": data,
                "pagination": {
                    "links": self.get_html_context(),
                    "current_page": self.get_page_number(
                        self.request, self.django_paginator_class
                    ),
                    "page_size": self.get_page_size(self.request),
                    "all_pages": self.page.paginator.num_pages,
                },
            }
        )
