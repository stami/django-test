from django.contrib import admin

from api.ips.models import UserIp


class UserIpAdmin(admin.ModelAdmin):

    list_display = [
        "id",
        "ip",
        "type",
        "continent_name",
        "country_name",
        "region_name",
        "city",
        "created_at",
    ]

    search_fields = ["id", "ip", "city"]
    list_filter = ["type"]


admin.site.register(UserIp, UserIpAdmin)
