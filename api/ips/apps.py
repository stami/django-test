from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class IpsConfig(AppConfig):
    name = "api.ips"
    verbose_name = _("IP's")

    def ready(self):
        try:
            import api.ips.signals  # noqa F401
        except ImportError:
            pass
