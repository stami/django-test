import uuid

from django.db import models
from django.db.models import JSONField, Q
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from api.utils.soft_deletable_qs import SoftDeletableQS


class UserIpManager(models.Manager):
    """
    UserIp Model Manager
    """

    def get_queryset(self):
        """returns all records - common use"""
        return (
            SoftDeletableQS(model=self.model, using=self._db, hints=self._hints)
            .filter(Q(deleted_at__isnull=True))
            .order_by("-created_at")
        )

    def get_deleted_ofers(self, **kwargs):
        """returns deleted Records"""
        return (
            SoftDeletableQS(model=self.model, using=self._db, hints=self._hints)
            .filter(deleted_at__isnull=False)
            .order_by("-created_at")
        )


class UserIp(models.Model):
    """
    Users IP's main model
    """

    objects = models.Manager()
    active_objects = UserIpManager()

    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False, unique=True
    )
    ip = models.CharField(max_length=40)

    TYPE_IPV4 = "ipv4"
    TYPE_IPV6 = "ipv6"
    TYPE_CHOICES = (
        (TYPE_IPV4, _("ipv4")),
        (TYPE_IPV6, _("ipv6")),
    )
    type = models.CharField(max_length=4, choices=TYPE_CHOICES, default=TYPE_IPV4)

    continent_name = models.CharField(max_length=20)
    country_name = models.CharField(max_length=40)
    region_name = models.CharField(max_length=40)
    city = models.CharField(max_length=40)
    zip = models.CharField(max_length=40, null=True, blank=True, default=None)
    longitude = models.DecimalField(max_digits=19, decimal_places=15)
    latitude = models.DecimalField(max_digits=19, decimal_places=15)
    location = JSONField(default=dict)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(null=True, blank=True, default=None)

    class Meta:
        verbose_name_plural = "UsersIP"

    def __str__(self):
        return str(self.id)[-12:]

    def delete(self):
        """Softly delete object"""
        self.deleted_at = timezone.now()
        self.save()

    def hard_delete(slef):
        """Remove the object from the database permanently"""
        super().delete()
