from rest_framework.serializers import ModelSerializer

from api.ips.models import UserIp


class IpLogsSerializer(ModelSerializer):
    class Meta:
        model = UserIp
        fields = "__all__"


class UserIpSerializer(ModelSerializer):
    class Meta:
        model = UserIp
        exclude = ["deleted_at"]
