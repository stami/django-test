from django.conf.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter

from api.ips.views import GetIpDataViewSet, LogsViewSet

router = DefaultRouter()
router.register(r"ips/get-data", GetIpDataViewSet, basename="get-ip-data")
router.register(r"ips/logs", LogsViewSet, basename="ip-logs")

app_name = "ips"
urlpatterns = [
    path("", include(router.urls)),
]
