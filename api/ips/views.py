from datetime import timedelta

import requests
from django.conf import settings
from django.utils import timezone
from rest_framework import status
from rest_framework.mixins import (
    CreateModelMixin,
    DestroyModelMixin,
    ListModelMixin,
    RetrieveModelMixin,
    UpdateModelMixin,
)
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from api.ips.models import UserIp
from api.ips.seralizers import IpLogsSerializer, UserIpSerializer


class LogsViewSet(
    GenericViewSet, ListModelMixin, DestroyModelMixin, RetrieveModelMixin
):
    serializer_class = IpLogsSerializer
    queryset = UserIp.active_objects.all()
    lookup_field = "id"
    lookup_value_regex = "[a-z0-9-]+"


class GetIpDataViewSet(GenericViewSet, CreateModelMixin, UpdateModelMixin):
    # TODO: Add class description for swagger

    permission_classes = [AllowAny]
    authentication_classes = []
    serializer_class = UserIpSerializer
    queryset = UserIp.active_objects.all()
    lookup_field = "id"
    lookup_value_regex = "[a-z0-9-]+"

    def initial(self, request, *args, **kwargs) -> None:
        requested_ip = request.query_params.get("ip", request.data.get("ip", None))
        # requested_ip = request.data.get("ip", None)

        self.stored_data = (
            UserIp.active_objects.filter(ip=requested_ip)
            .order_by("-created_at")
            .first()
        )

        return super().initial(request, *args, **kwargs)

    def __add_record(self, data):
        "Add record to database"
        item = UserIp(
            ip=data["ip"],
            type=UserIp.TYPE_IPV4 if data["type"] == "ipv4" else UserIp.TYPE_IPV6,
            continent_name=data["continent_name"],
            country_name=data["country_name"],
            region_name=data["region_name"],
            city=data["city"],
            zip=data["zip"],
            longitude=data["longitude"],
            latitude=data["latitude"],
            location=data["location"],
        )
        item.save()

        return item

    def __check_online(self):
        "Online check only if no record in DB or record is older than 1d"
        now = timezone.now()
        if self.stored_data is None or (
            self.stored_data and self.stored_data.created_at < (now - timedelta(days=1))
        ):
            return True
        return False

    def __get_ip_data(self, ip_to_check, force_online_check=False):
        data = None
        if self.__check_online() or force_online_check:
            res = requests.get(
                "http://api.ipstack.com/{}?access_key={}".format(
                    ip_to_check, settings.IPSTACK_API_KEY
                )
            )
            if res.status_code == 200:
                data = res.json()
        else:
            serializer = self.get_serializer(self.stored_data)
            data = serializer.data

        return data

    def create(self, request, *args, **kwargs):
        """
        Getting IP info
        """
        if "ip" in request.data:

            ip_data = self.__get_ip_data(request.data["ip"])
            stored_data = self.__add_record(ip_data)
            serializer = self.get_serializer(stored_data)

            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(
            {"detail", "EIP01: Please provide IP to check"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    def update(self, request, *args, **kwargs):
        """
        Update IP info
        """
        if "ip" in request.data:
            item = self.get_object()
            ip_data = self.__get_ip_data(
                ip_to_check=request.data["ip"], force_online_check=True
            )
            serializer = self.get_serializer(item, ip_data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(
            {"detail", "EIP01: Please provide IP to check"},
            status=status.HTTP_400_BAD_REQUEST,
        )
