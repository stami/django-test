# 1.0.2 (2022-02-14)

## Features

- __IPS__:
  - Update log [Tree](https://gitlab.com/stami/django-test/-/merge_requests/9)
  - Delete log [Tree](https://gitlab.com/stami/django-test/-/merge_requests/10)
  - Read log [Tree](https://gitlab.com/stami/django-test/-/merge_requests/11)


# 1.0.1 (2022-02-06)

## Features

- __User__: Logged User Endpoint  [Tree](https://gitlab.com/stami/django-test/-/merge_requests/5)
- __IPS__: IPS App with base endpoints [Tree](https://gitlab.com/stami/django-test/-/merge_requests/6)


# 1.0.0 (2022-02-03)

## Features

- __ENV__: Base environment configuration [Tree](https://gitlab.com/stami/django-test/-/merge_requests/1)
- __Custom User__: Add Custom user model  [Tree](https://gitlab.com/stami/django-test/-/merge_requests/1)
- __User Authenticate__: Add User DRF Authentication with JWT Token [Tree](https://gitlab.com/stami/django-test/-/merge_requests/1)
