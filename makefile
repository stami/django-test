dev:
	docker-compose -p sofomo -f local.yml up --build

migrations:
	docker-compose -p sofomo -f local.yml run --rm django python manage.py makemigrations

tests:
	docker-compose -p sofomo -f local.yml run --rm django pytest -x

clean:
	pre-commit run --all-files
